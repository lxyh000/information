from flask_script import Manager  # 导入管理对象
from flask_migrate import Migrate, MigrateCommand  # 导入迁移对象
import pymysql
from info import create_app, db
from info.models import User
import info.models

pymysql.install_as_MySQLdb()

"""从单一职责的角度思考：manage.py文件专门用来做项目的启动文件即可，其他配置放到别的文件"""

app = create_app("development")  # 工厂c思想创建对象

# 6 创建管理对象
manage = Manager(app)

# 7 创建迁移对象
migrate = Migrate(app, db)

# 8 添加迁移命令
manage.add_command("db", MigrateCommand)


@manage.option("-n", "-name", dest="name")
@manage.option("-m", "-password", dest="password")
def create_superuser(name, password):
    """创建管理员用户"""
    if not all([name, password]):
        print('参数不足')
        return

    user = User()
    user.mobile = name
    user.nick_name = name
    user.password = password
    user.is_admin = True

    try:
        db.session.add(user)
        db.session.commit()
        print("创建成功")
    except Exception as e:
        print(e)
        db.session.rollback()


if __name__ == '__main__':
    manage.run()
