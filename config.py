from redis import StrictRedis  # 导入创建redis对象
import logging  # 导入日志类


class Config(object):
    """配置类（父类）"""
    DEBUG = True
    SECRET_KEY = "ASLKDJALKSJDALSDJALKSDJASLKDJ98ADU9"

    # 数据库的配置信息
    SQLALCHEMY_DATABASE_URI = "mysql://root:gwh19971563@127.0.0.1:3306/information"  # 数据库连接配置
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # 关闭数据库修改跟踪

    # redis数据库配置
    REDIS_HOST = "127.0.0.1"
    REDIS_PORT = 6379
    REDIS_NUM = 0

    # 利用flask_session拓展包将flask中的session存储位置从内存更改到redis服务器中

    # 存储到数据库类型
    SESSION_TYPE = "redis"
    # 初始化一个
    SESSION_REDIS = StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_NUM)
    # 取消永久存储
    SESSION_PERMANENT = False
    # 开始session的加密操作
    SESSION_USE_SIGNER = True
    # 设置默认有效时长
    PERMANENT_SESSION_LIFETIME = 86400


class DevelopmentConfig(Config):
    """开发环境的配置类"""
    DEBUG = True
    # 设置开发环境的日志级别为：DEBUG
    LOG_LEVEL = logging.DEBUG


class ProductionConfig(Config):
    """线上环境的配置类"""
    DEBUG = False
    # 设置线上环境的日志级别为：WARNING
    LOG_LEVEL = logging.WARNING


config_dict = {"development": DevelopmentConfig,
               "production": ProductionConfig}
