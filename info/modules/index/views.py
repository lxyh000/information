import logging
from flask import current_app, request, jsonify, g

from info.utits.response_code import RET
from . import index_bp
from info import redis_store
from flask import render_template
from flask import session
from info.models import User, News, Category
from info import constants
from info.utits.common import user_login_data


# 首页展示
# 首页的实现（用户的登录信息，点击排行信息，新闻分类展示信息），请求方式为get
@index_bp.route('/')
@user_login_data
def hello_world():
    # --------------------------判断用户登录状态--------------------

    # return render_template("news/index.html")
    # 获取到当前登录用户的id
    # user_id = session.get("user_id")  # 数据怎么存的最好怎么取
    # # 通过id获取用户信息
    # # user_id = redis_store.get("user_id")  # (这种方法好像取不到数据)
    # user = None
    # if user_id:
    #     try:
    #         user = User.query.get(user_id)
    #     except Exception as e:
    #         current_app.logger.error(e)
    user = g.user

    # --------------------------点击排行展示-------------------------

    # 根据数据库中的点击数量进行排序
    news_list = None
    try:
        news_list = News.query.order_by(News.clicks.desc()).limit(constants.CLICK_RANK_MAX_NEWS)
    except Exception as e:
        current_app.logger.error(e)

    click_news_list = []
    for news in news_list if news_list else []:
        click_news_list.append(news.to_basic_dict())

    # -------------------------新闻分类展示------------------------------

    categories = Category.query.all()
    categories_dicts = []

    for category in categories:  # (按答案写枚举类型会报错)
        categories_dicts.append(category.to_dict())

    data = {
        "user_info": user.to_dict() if user else None,
        "click_news_list": click_news_list,
        "categories": categories_dicts
    }
    return render_template('news/index.html', data=data)


# 发送图标
# 这只是一个调用小图标的功能，请求方式为get
@index_bp.route("/favicon.ico")
def get_ico():
    # 这是发送浏览器访问图标的文件（定时请求）
    return current_app.send_static_file('news/favicon.ico')  # 调用发送静态文件的方法


# 查询分类数据
# 用来从数据库查询每个分类的数据，请求方式为get
@index_bp.route("/news_list")
def get_news_list():
    """
        获取指定分类的新闻列表
        1. 获取参数
        2. 校验参数
        3. 查询数据
        4. 返回数据
        :return:
        """
    # 获取参数
    args_dict = request.args
    # 参数后面的值为默认值
    page = args_dict.get("page", '1')
    per_page = args_dict.get("per_page", constants.HOME_PAGE_MAX_NEWS)  #
    category_id = args_dict.get("cid", '1')

    # 校验参数(在这需要字符串类类型的页数转化为整型)
    try:
        page = int(page)
        per_page = int(per_page)
        cid = int(category_id)
    except Exception as e:
        current_app.logger(e)
        return jsonify({"errno": RET.PARAMERR, "errmsg": "参数错误"})

    # 查询分类的id并进行分页

    # 因为最新是按时间先后排序的，而其他的是按分类来查询的，所以要分情况讨论

    """
    这是一种普通方法：
    
    if cid == 1:
    # 只需要根据新闻创建时间的降序排序
        paginate = News.query.order_by(News.create_time.desc()).paginate(page, per_page, False)
    else:
    # 需要根据新闻创建时间的降序排序和分类id
        paginate = News.query.filter(News.category_id == cid).order_by(News.create_time.desc()).paginate(page, per_page, False)
    """

    # 定义一个列表，保存查询时多出的那个条件

    filters = [News.status == 0]
    if cid != 1:
        filters.append(News.category_id == cid)  # 将多出要查询的这个条件添加到列表中

    # 接下进行分页查询（查询的加一步解包）
    try:
        paginate = News.query.filter(*filters).order_by(News.create_time.desc()).paginate(page, per_page, False)
        # 当前页的所有数据
        items = paginate.items
        # 当前的页数
        current_page = paginate.page
        # 总页数
        total_page = paginate.pages
    except Exception as e:
        current_app.logger(e)
        return jsonify({"errno": RET.DBERR, "errmsg": "数据库查询错误"})

    # 将查询的结果，对象列表转字典列表
    news_dict_list = []
    for news in items if items else []:
        news_dict_list.append(news.to_dict())

    # 组织返回数据
    data = {
        "news_list": news_dict_list,
        "current_page": current_page,
        "total_page": total_page
    }

    return jsonify(errno=RET.OK, errmsg="查询新闻列表数据成功", data=data)
