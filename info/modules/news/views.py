from info import constants, db
from info.models import User, News, Comment, CommentLike
from info.modules.news import news_bp
from flask import render_template, session, current_app, jsonify, g, request
from info.utits.common import user_login_data
from info.utits.response_code import RET


# 点赞
# 对新闻进行点赞与取消点赞的功能，并将结果保存到数据库，请求方式为post
# 返回前端ok的数据，至于点赞的个数，前端的回调函数会自己实现（这里只要根据不同的情况，进行点赞与取消点赞就可以）
# 点赞后要立即显示出来，需要回调函数去操作
@news_bp.route("/comment_like", methods=["POST"])
@user_login_data
def comment_like():
    # 首先看数据是根据什么方式发送的（不同方式要记得取的方式）
    params_dict = request.json
    comment_id = params_dict.get("comment_id")
    action = params_dict.get("action")
    user = g.user

    # 进行非空判断
    # 一般这步非空判断就是对取出的发送的数据进行检验
    if not all([comment_id, action]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数不足")
    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")

    # 2.3 action in ["add", "remove"]
    if action not in ["add", "remove"]:
        return jsonify(errno=RET.PARAMERR, errmsg="action参数错误")

    # 当校验结束后，一般再去根据一些逻辑判断去操作数据库，最后把这些逻辑数据结束后，就可以返回前端需要的数据
    try:
        comment = Comment.query.get(comment_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询数据错误")

    if not comment:
        return jsonify(errno=RET.NODATA, errmsg="该评论不存在")

    if action == "add":
        comment_like = CommentLike.query.filter_by(comment_id=comment_id, user_id=g.user.id).first()
        if not comment_like:
            comment_like = CommentLike()
            comment_like.comment_id = comment_id
            comment_like.user_id = g.user.id
            db.session.add(comment_like)
            # 增加点赞条数
            comment.like_count += 1
    else:
        # 删除点赞数据
        comment_like = CommentLike.query.filter_by(comment_id=comment_id, user_id=g.user.id).first()
        if comment_like:
            db.session.delete(comment_like)
            # 减小点赞条数
            comment.like_count -= 1
    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="操作失败")
    return jsonify(errno=RET.OK, errmsg="操作成功")


# 评论
# 这里把评论的内容保存到数据库，然后把这条评论返回到回调函数进行展示（我认为这地方没必要返回，前端可以直接进行拼接）
# 评论完之后也要立即显示出来，需要回调函数去操作（并且更新评论条数什么的，都直接在回调函数中进行就行）
@news_bp.route("/news_comment", methods=["POST"])
@user_login_data
def news_comment():
    """发布评论接口（主评论，子评论）"""
    """
    1.获取参数
        1.1 comment_str:评论内容，news_id:新闻id， parent_id：父评论id（非必传参数）
    2.校验参数
        2.1 非空判断
    3.逻辑处理
        3.0 根据news_id查询新闻是否存在
        3.1 parent_id没有值：发布主评论
        3.2 parent_id有值：发布子评论
    4.返回值
    """
    # 1.1 comment_str:评论内容，news_id:新闻id， parent_id：父评论id（非必传参数）
    params_dict = request.json
    comment_str = params_dict.get("comment")
    news_id = params_dict.get("news_id")
    parent_id = params_dict.get("parent_id")
    # 获取用户对象
    user = g.user

    # 2.1 非空判断
    if not all([news_id, comment_str]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数不足")

    # 2.2 用户是否登录判断
    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")

    # 3.0 根据news_id查询新闻是否存在
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询新闻对象异常")
    if not news:
        return jsonify(errno=RET.NODATA, errmsg="新闻不存在")

    # 3.1 parent_id没有值：发布主评论
    comment_obj = Comment()
    comment_obj.user_id = user.id
    comment_obj.news_id = news_id
    comment_obj.content = comment_str
    # 3.2 parent_id有值：发布子评论
    if parent_id:
        comment_obj.parent_id = parent_id

    # 3.3 保存到数据库
    try:
        db.session.add(comment_obj)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="保存评论对象异常")

    # 4.返回值
    return jsonify(errno=RET.OK, errmsg="评论成功", data=comment_obj.to_dict())


# 收藏
# 这里和之前差不多的样子，都是用户直接进行点击或类似的操作，然后调用js中的函数来发起请求
# 这里是根据用户发送的是收藏还是取消收藏，进行数据库的操作，这里其实也不用返回数据，收藏的与否直接交给前端操作就可以
# 这里只要对数据库进行操作就可以了
@news_bp.route("/news_collect", methods=["POST"])
@user_login_data
def news_collect():
    """用户点击收藏或取消收藏"""

    # 1.获取参数
    #     1.1 news_id：新闻id, action:表示收藏和取消收藏的行为（'collect', 'cancel_collect'）
    # 2.校验参数
    #     2.1 非空判断
    #     2.2 action必须是在['collect', 'cancel_collect']
    # 3.逻辑处理
    #     3.0 根据news_id查询该新闻
    #     3.1 action是collect表示收藏: 将新闻添加到user.collection_news列表中
    #     3.2 action是cancel_collect表示取消收藏: 将新闻从user.collection_news列表中移除
    # 4.返回值

    user = g.user
    # 1 获取参数
    param_dict = request.json

    news_id = param_dict.get("news_id")
    action = param_dict.get("action")

    # 2 进行参数校验
    if not user:
        return jsonify({"errno": RET.SESSIONERR, "errmsg": "用户未登录"})

    if not all([news_id, action]):
        return jsonify({"errno": RET.PARAMERR, "errmsg": "参数不完整"})

    if action not in ['collect', 'cancel_collect']:
        return jsonify(errno=RET.PARAMERR, errmsg="参数内容错误")

    # 根据new_id进行查询数据，进行分析，是应该收藏还是取消收藏
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify({"errno": RET.DBERR, "errmsg": "数据库查询错误"})

    # 校验查询出来的新闻
    if not news:
        return jsonify({"errno": RET.NODATA, "errmsg": "该新闻不存在"})

    # 根据action判断是收藏还是取消收藏
    if action == "collect":
        user.collection_news.append(news)
    else:
        user.collection_news.remove(news)

    # 修改之后进行提交操作
    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify({"errno": RET.DBERR, "errmsg": "修改数据库失败"})

    return jsonify(errno=RET.OK, errmsg="OK")


# 主页展示
# 这里是详情页的内容展示，为get请求，即为查询数据
# 需要展示出（用户的登录状态， 点击排行展示， 新闻内容的查询，该新闻是否收藏，获取该新闻的评论，查询该新闻的点赞数）
# 这里只是一个基本数据的展示，这里需要的操作是从数据库查处需要展示的数据，然后返回前端，交给前端进行操作
# http://127.0.0.1:5000/news/1
@news_bp.route("/<int:news_id>")
@user_login_data
def get_detail(news_id):
    """展示新闻详情页"""

    # --------------------------判断用户登录状态--------------------

    # return render_template("news/index.html")
    # 获取到当前登录用户的id
    # user_id = session.get("user_id")  # 数据怎么存的最好怎么取
    # # 通过id获取用户信息
    # # user_id = redis_store.get("user_id")  # (这种方法好像取不到数据)
    # user = None
    # if user_id:
    #     try:
    #         user = User.query.get(user_id)
    #     except Exception as e:
    #         current_app.logger.error(e)
    user = g.user

    # --------------------------点击排行展示-------------------------

    # 根据数据库中的点击数量进行排序
    news_list = None
    try:
        news_list = News.query.order_by(News.clicks.desc()).limit(constants.CLICK_RANK_MAX_NEWS)
    except Exception as e:
        current_app.logger.error(e)

    click_news_list = []
    for news in news_list if news_list else []:
        click_news_list.append(news.to_basic_dict())

    # --------------------------新闻详情页面查询-------------------------

    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify({"errno": RET.DBERR, "errmsg": "数据库查询错误"})

    news_list = news.to_dict()
    # --------------------------查询用户是否收藏该数据-------------------------
    is_collected = False
    if user:
        if news in user.collection_news:
            is_collected = True

    # --------------------------获取该新闻的评论-------------------------
    try:
        comments = Comment.query.filter(Comment.news_id == news_id).order_by(Comment.create_time.desc()).all()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询数据库错误")

    # --------------------------查询当前新闻的评论那几条点过赞-------------------------

    comment_id_list = [comment.id for comment in comments]
    if user:
        try:
            commentlike_model_list = CommentLike.query.filter(CommentLike.comment_id.in_(comment_id_list),
                                                              CommentLike.user_id == user.id).all()
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.DBERR, errmsg="查询评论点赞模型对象异常")
        commentlike_id_list = [commentlike_model.comment_id for commentlike_model in commentlike_model_list]

        comment_list = []
        for comment in comments if comments else []:
            comment_dict = comment.to_dict()
            comment_dict["is_like"] = False
            if comment.id in commentlike_id_list:
                comment_dict["is_like"] = True
            comment_list.append(comment_dict)
    else:
        comment_list = []
        for comment in comments if comments else []:
            comment_dict = comment.to_dict()
            comment_list.append(comment_dict)

    # --------------------------查询当前是否关注此用户-------------------------

    # 当前登录用户是否关注当前新闻作者(前端的判断功能还未实现)
    is_followed = False
    # is_collected = False
    # 判断用户是否存在，如果存在看新闻是否在收集的新闻中
    if news.user:
        # if news in g.user.collection_news:
        #     is_collected = True
        if news.user.followers.filter(User.id == g.user.id).count() > 0:
            is_followed = True

    # if user:
    #     if news.user:
    #         if news.user.followers.filter(User.id == g.user.id).count() > 0:
    #             is_followed = True

    data = {
        "user_info": user.to_dict() if user else None,
        "click_news_list": click_news_list,
        "news": news_list,
        "is_collected": is_collected,
        "comments": comment_list,
        "is_followed": is_followed

    }

    return render_template("news/detail.html", data=data)


# 关注与取消关注
@news_bp.route("/followed_user", methods=["POST"])
@user_login_data
def followed_user():
    """关注与取消关注"""

    if not g.user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")
    # 获取参数
    user_id = request.json.get("user_id")
    action = request.json.get("action")

    # 参数校验
    if not all([user_id, action]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数不足")

    if action not in ("follow", "unfollow"):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    # 根据用户去查用户是否存在
    try:
        target_user = User.query.get(user_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")

    if not target_user:
        return jsonify(errno=RET.NODATA, errmsg="要关注的用户不存在")

    # 用户存在的情况下，根据action的值判断是关注还是取消关注
    # 移出与添加的好像都是整个对象
    if action == "follow":
        if target_user.followers.filter(User.id == g.user.id).count() > 0:
            return jsonify(errno=RET.DATAEXIST, errmsg="当前已关注")
        target_user.followers.append(g.user)
    else:
        if target_user.followers.filter(User.id == g.user.id).count() > 0:
            target_user.followers.remove(g.user)

    # 保存到数据库中
    try:
        db.session.commit()
    except Exception as e:
        current_app.logger(e)
        return jsonify(errno=RET.DBERR, errmsg="保存到数据库错误")
    return jsonify(errno=RET.OK, errmsg="修改成功")
