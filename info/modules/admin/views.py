import time
from datetime import datetime, timedelta
from info.models import User, News, Category
from info.modules.admin import admin_bp
from flask import render_template, request, jsonify, current_app, session, g, url_for, redirect
from info.utits.common import user_login_data
from info.utits.response_code import RET
from info import constants, db
from info.utits import pic_storage


@admin_bp.route("/login", methods=["POST", "GET"])
def admin_login():
    # 如果是直接请求这个页面时就直接返回数据
    if request.method == "GET":
        # 去 session 中取指定的值
        user_id = session.get("user_id", None)
        is_admin = session.get("is_admin", False)
        # 如果用户id存在，并且是管理员，那么直接跳转管理后台主页
        if user_id and is_admin:
            return redirect(url_for('admin.admin_index'))
        return render_template('admin/login.html')

    # 如果是提交表单数据就要进行数据处理u
    else:
        # 获取数据
        password = request.form.get("password")
        username = request.form.get("username")

        if not all([password, username]):
            return render_template('admin/login.html', errmsg="参数不足")

        # 进行数据库查询
        try:
            user = User.query.filter(User.mobile == username).first()
        except Exception as e:
            current_app.logger.error(e)
            return render_template('admin/login.html', errmsg="数据库查询错误")

        if not user:
            return render_template('admin/login.html', errmsg="账号不存在")

        # 这是user的一个方法，可以直接去验证密码
        if not user.check_passowrd(password):
            return render_template('admin/login.html', errmsg="密码错误")

        if not user.is_admin:
            return render_template('admin/login.html', errmsg="权限错误")

        # 保持登录状态
        session["user_id"] = user.id
        session["nick_name"] = user.nick_name
        session["mobile"] = user.mobile
        session["is_admin"] = True

        return redirect(url_for('admin.admin_index'))


@admin_bp.route("/")
@user_login_data
def admin_index():
    """管理员首页"""
    user = g.user
    is_admin = session.get("is_admin", False)
    if not is_admin:
        return redirect(url_for("admin.admin_login"))

    # 优化进入主页逻辑：如果管理员从普通界面想要进入管理员界面，直接登录，不是管理员，就引导到登录界面
    if not user:
        return render_template(url_for('admin.admin_login'))

    # 构建数据
    context = {
        'user': user.to_dict()
    }
    return render_template('admin/index.html', data=context)


# 用户统计
@admin_bp.route("/user_count")
def user_count():
    """查询总人数"""
    total_count = 0
    try:
        total_count = User.query.filter(User.is_admin == False).count()
    except Exception as e:
        current_app.logger.error(e)

        # 查询月新增数
    mon_count = 0
    try:
        now = time.localtime()
        mon_begin = '%d-%02d-01' % (now.tm_year, now.tm_mon)
        mon_begin_date = datetime.strptime(mon_begin, '%Y-%m-%d')
        mon_count = User.query.filter(User.is_admin == False, User.create_time >= mon_begin_date).count()
    except Exception as e:
        current_app.logger.error(e)

    # 查询日新增数
    day_count = 0
    try:
        now = time.localtime()  # 现在的时间
        day_begin = '%d-%02d-%02d' % (now.tm_year, now.tm_mon, now.tm_mday)
        day_begin_date = datetime.strptime(day_begin, '%Y-%m-%d')
        day_count = User.query.filter(User.is_admin == False, User.create_time > day_begin_date).count()
    except Exception as e:
        current_app.logger.error(e)

    # 获取图标信息
    # 获取到当天00：00：00时间
    now_date = datetime.strptime(datetime.now().strftime('%Y-%m-%d'), '%Y-%m-%d')
    # 定义空数组，保存数据
    active_date = []
    active_count = []

    # 依次添加数据，再反转
    for i in range(0, 31):
        begin_date = now_date - timedelta(days=i)
        end_date = now_date - timedelta(days=(i - 1))
        active_date.append(begin_date.strftime('%Y-%m-%d'))
        count = 0
        try:
            count = User.query.filter(User.is_admin == False, User.last_login >= begin_date,
                                      User.last_login < end_date).count()
        except Exception as e:
            current_app.logger.error(e)
        active_count.append(count)

    active_date.reverse()
    active_count.reverse()

    data = {"total_count": total_count,
            "mon_count": mon_count,
            "day_count": day_count,
            "active_date": active_date,
            "active_count": active_count}

    return render_template('admin/user_count.html', data=data)


# 用户列表
@admin_bp.route("/user_list")
def user_list():
    """获取用户列表"""
    # 获取参数
    page = request.args.get("p", 1)

    # 将页数进行int化
    try:
        page = int(page)
    except Exception as e:
        current_app.logger.error(e)
        page = 1

    # 去数据库去查询用户列表数据
    user_list = []
    current_page = 1
    total_page = 1
    try:
        paginate = User.query.filter(User.is_admin == False).order_by(User.create_time.desc()).paginate(page,
                                                                                                        constants.ADMIN_USER_PAGE_MAX_COUNT,
                                                                                                        False)
        user_list = paginate.items
        current_page = paginate.page
        total_page = paginate.pages

    except Exception as e:
        current_app.logger.error(e)

    # 将模型转化为字典

    user_dict = []
    for user in user_list:
        user_dict.append(user.to_admin_dict())

    data = {
        "users": user_dict,
        "current_page": current_page,
        "total_page": total_page
    }

    return render_template("admin/user_list.html", data=data)


# 新闻审核
@admin_bp.route("/news_review")
def news_review():
    # 获取参数
    page = request.args.get("p", 1)
    keywords = request.args.get("keywords", "")  # 这个地方为什么是get请求，不太懂
    try:
        page = int(page)
    except Exception as e:
        current_app.logger.error(e)
        page = 1

    # 进入数据库进行查询
    news_list = []
    current_page = 1
    total_page = 1
    filters = [News.status != 0]
    if keywords:
        filters.append(News.title.contains(keywords))  # 这里有个新语法contains，什么有没有什么东西
    try:
        paginate = News.query.filter(*filters).order_by(News.create_time.desc()).paginate(page,
                                                                                          constants.ADMIN_NEWS_PAGE_MAX_COUNT,
                                                                                          False)
        news_list = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)

    # 新闻数据转为列表
    news_dict = []
    for news in news_list:
        news_dict.append(news.to_review_dict())

    data = {
        "total_page": total_page,
        "current_page": current_page,
        "news_list": news_dict
    }

    return render_template("admin/news_review.html", data=data)


# 新闻审核详情界面
@admin_bp.route("/news_review_detail", methods=["POST", "GET"])
def news_review_detail():
    # 详情页面应该有直接访问的时候，和提交post请求的时候
    if request.method == "GET":
        # news_id = request.args.get("news_id")
        news_id = request.args.get("news_id")

        if not news_id:
            return render_template("admin/news_review_detail.html", data={"errmsg": "未查询到此新闻"})

        # 通过id去数据库中新闻
        news = None
        try:
            news = News.query.get(news_id)
        except Exception as e:
            current_app.logger.error(e)

        if not news:
            return render_template('admin/news_review_detail.html', data={"errmsg": "未查询到此新闻"})
        data = {
            "news": news.to_dict()
        }
        return render_template('admin/news_review_detail.html', data=data)

    else:

        # 获取参数
        news_id = request.json.get("news_id")
        action = request.json.get("action")

        # 参数校验
        if not all([news_id, action]):
            return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

        if action not in ("accept", "reject"):
            return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

        try:
            news = News.query.get(news_id)
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.DBERR, errmsg="查询数据库错误")

        if not news:
            return jsonify(errno=RET.NODATA, errmsg="没有该新闻")

        # 根据不同的操作判断将要进行什么操作
        if action == "accept":
            news.status = 0
        else:
            reason = request.json.get("reason")
            if not reason:
                return jsonify(errno=RET.PARAMERR, errmsg="参数不足")
            news.reason = reason
            news.status = -1

        # 因为对数据库进行了修改，所以需要提交数据库
        try:
            db.session.commit()
        except Exception as e:
            current_app.logger.error(e)
            db.session.rollback()
            return jsonify(errno=RET.DBERR, errmsg="数据库保存错误")

        return jsonify(errno=RET.OK, errmsg="操作成功")


# 新闻板式编辑
@admin_bp.route("/news_edit")
def news_edit():
    """直接返回新闻列表（还有一个新闻搜索功能）"""
    # 获取参数
    page = request.args.get("p", 1)
    keywords = request.args.get("keywords", "")  # 这个字段是可能有的

    try:
        page = int(page)
    except Exception as e:
        current_app.logger.error(e)
        page = 1

    filters = []
    if keywords:
        filters.append(News.title.contains(keywords))
    # 数据查询数据
    news_list = []
    current_page = 1
    total_page = 1
    try:
        paginate = News.query.filter(*filters).order_by(News.create_time.desc()).paginate(page,
                                                                                          constants.ADMIN_NEWS_PAGE_MAX_COUNT,
                                                                                          False)
        news_list = paginate.items
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)

    news_dict = []
    for news in news_list:
        news_dict.append(news.to_basic_dict())

    data = {
        "current_page": current_page,
        "total_page": total_page,
        "news_list": news_dict
    }

    return render_template('admin/news_edit.html', data=data)


# 新闻板式编辑详情页
@admin_bp.route("/news_edit_detail", methods=["POST", "GET"])
def news_edit_detail():
    if request.method == "GET":
        # 首先获取参数
        news_id = request.args.get("news_id")
        if not news_id:
            return render_template("admin/news_edit_detail.html", data={"errmsg": "未查询到此新闻"})

        news = None
        try:
            news = News.query.get(news_id)
        except Exception as e:
            current_app.logger.error(e)

        # 当查询到新闻之后，再查出新闻的分类，然后进行数据返回
        if not news:
            return render_template("admin/news_edit_detail.html", data={"errmsg": "新闻不存在"})

        # 查询到分类后，还要把该新闻默认的类型的勾选
        try:
            categories = Category.query.all()
        except Exception as e:
            current_app.logger.error(e)
            return render_template("admin/news_edit_detail.html", data={"errmsg": "数据库查询错误"})

        categories_li = []
        for category in categories:
            c_dict = category.to_dict()
            # 因为要看有没有被选择，所以要再加一个字典key
            c_dict["is_selected"] = False
            if category.id == news.category_id:
                c_dict["is_selected"] = True
            categories_li.append(c_dict)

        categories_li.pop(0)
        data = {
            "news": news.to_dict(),
            "categories": categories_li
        }

        return render_template('admin/news_edit_detail.html', data=data)
    else:
        # 当为post请求时，也就是相当于提交一条新闻，要获取此条新闻的好多内容

        # 获取参数
        news_id = request.form.get("news_id")
        title = request.form.get("title")
        digest = request.form.get("digest")
        content = request.form.get("content")
        index_image = request.files.get("index_image")
        category_id = request.form.get("category_id")

        # 进行非空判断
        if not all([title, digest, content, category_id]):
            return jsonify(errno=RET.PARAMERR, errmsg="参数不足")

        # 还要去查询这条新闻是不是存在，然后获取到的图片要上传到七牛云中
        news = None
        try:
            news = News.query.get(news_id)
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")

        if not news:
            return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

            # 1.2 尝试读取图片
        if index_image:
            try:
                index_image = index_image.read()
            except Exception as e:
                current_app.logger.error(e)
                return jsonify(errno=RET.PARAMERR, errmsg="参数有误")

            # 2. 将标题图片上传到七牛
            try:
                key = pic_storage(index_image)
            except Exception as e:
                current_app.logger.error(e)
                return jsonify(errno=RET.THIRDERR, errmsg="上传图片错误")
            news.index_image_url = constants.QINIU_DOMIN_PREFIX + key

        # 3. 设置相关数据
        news.title = title
        news.digest = digest
        news.content = content
        news.category_id = category_id

        # 4. 保存到数据库
        try:
            db.session.commit()
        except Exception as e:
            current_app.logger.error(e)
            db.session.rollback()
            return jsonify(errno=RET.DBERR, errmsg="保存数据失败")
        # 5. 返回结果
        return jsonify(errno=RET.OK, errmsg="编辑成功")


# 获取新闻分类
@admin_bp.route("/news_category")
def news_category():
    categories = Category.query.all()
    categories_dicts = []

    for category in categories:
        # 获取字典
        cate_dict = category.to_dict()
        # 拼接内容
        categories_dicts.append(cate_dict)

    categories_dicts.pop(0)
    # 返回内容
    return render_template('admin/news_type.html', data={"categories": categories_dicts})


# 增加新闻分类
@admin_bp.route('/add_category', methods=["POST"])
def add_category():
    category_id = request.json.get("id")
    category_name = request.json.get("name")
    if not category_name:
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")
    # 判断是否有分类id
    if category_id:
        try:
            category = Category.query.get(category_id)
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.DBERR, errmsg="查询数据失败")

        if not category:
            return jsonify(errno=RET.NODATA, errmsg="未查询到分类信息")

        category.name = category_name
    else:
        # 如果没有分类id，则是添加分类
        category = Category()
        category.name = category_name
        db.session.add(category)

    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="保存数据失败")
    return jsonify(errno=RET.OK, errmsg="保存数据成功")


# 用户关注列表的实现
@admin_bp.route("/user_follow")
@user_login_data
def get_user_follow():
    # 获取参数
    user = g.user
    page = request.args.get("p", 1)

    try:
        page = int(page)
    except Exception as e:
        current_app.logger.error(e)
        p = 1

    follows = []
    current_page = 1
    total_page = 1
    # 查询数据
    try:
        paginate = user.followed.paginate(page, constants.USER_FOLLOWED_MAX_COUNT, False)
        follows = paginate.items
        # 获取当前页
        current_page = paginate.page
        # 获取总页数
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)

    follows_dict = []
    for follow in follows:
        follows_dict.append(follow.to_dict())

    data = {
        "users": follows_dict,
        "total_page": total_page,
        "current_page": current_page}
    return render_template('profile/user_follow.html', data=data)
