from datetime import datetime

from info.modules.passport import passport_bp
from flask import request, abort, current_app, make_response, jsonify, session, redirect, url_for
from info.utits.captcha.captcha import captcha
from info import redis_store, db
from info.utits.response_code import RET
import re
from info.models import User
import random
from info import constants
from info.lib.yuntongxun.sms import CCP


# 用户登出
# 当用户点击登出时，调用js的登出函数，然后来请求后端，后端将保持登录的redis数据库的用户数据删除
# 后端只要返回ok，然后前端进行刷新页面，前端的模版语法就可以直接判断redis中的数据是否存在，然后进行展示
# 其他地方查询用户是否存在时，好像也可以这样不用返回数据，因为前端可以直接进行判断（这只是我的理解，可能后端来写一下更加严谨吧）
@passport_bp.route("/login_out", methods=["POST"])
def logout():
    """登出操作删除redis缓存中的用户数据可以了，因为每次前端刷新页面时，是重新刷新首页的路由函数，然后每次会在那里判断redis数据库中的内容"""
    # 使用谁存的数据就用谁去操作（因为存数据的都是通过session1去操作的，所以使用session去删除）
    session.pop("user_id", None)
    session.pop("nick_bane", None)
    session.pop("mobile", None)
    # 每次用户退出时都清除数据
    session.pop("is_admin", None)
    # 如果就删除，如果没有就会返回空值，不会报错
    return jsonify({"errno": RET.OK, "errmsg": "OK"})


# 用户登录
# 用户登录其实和登出是一个性质的，用户登录就是就行一系列校验，然后将数据保存到redis数据库
# 然后前端进行刷新界面就可以了（其实我感觉几乎所有的操作都可以通过，前端请求后端操作数据库，然后ok后前端刷新界面进行，不过这样太耗资源，使用ajax的好处就是可以局部刷新）
@passport_bp.route("/login", methods=["POST"])
def login():
    # 获取参数
    json_data = request.json

    mobile = json_data.get("mobile")
    password = json_data.get("password")

    # 校验参数
    if not all([mobile, password]):
        return jsonify({"errno": RET.PARAMERR, "errmsg": "参数不足"})

    # 从数据库中取出要登录的用户
    try:
        user = User.query.filter(User.mobile == mobile).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify({"errno": RET.DBERR, "errmsg": "数据库中取数据发生错误"})

    if not user:
        return jsonify({"errno": RET.USERERR, "errmsg": "用户不存在"})

    if not user.check_passowrd(password):
        return jsonify({"errno": RET.PARAMERR, "errmsg": "密码错误"})

    # 4. 保存用户登录状态
    session["user_id"] = user.id
    session["nick_name"] = user.nick_name
    session["mobile"] = user.mobile
    # 记录用户最后一次登录时间
    # 在登录的时候添加一个判断条件，如果超级用户才会在session中添加is_admin（在进入管理员界面时就可以进行判断）
    if user.is_admin:
        session["is_admin"] = True
    user.last_login = datetime.now()
    try:
        # 修改数据直接提交，不需要取添加
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        # 5. 登录成功
    return jsonify(errno=RET.OK, errmsg="OK")


# 注册提交
# 注册也是前端点击，发送请求来后端，后端进行参数校验，将注册的数据保存到数据库，然后将保持登录的数据保存到redis中
# 返回ok后，前端进行刷新页面
@passport_bp.route("/register", methods=["POST"])
def register():
    """
    1 提取参数
    2 参数验证
    3 逻辑分析
    4 返回数据
    """
    # 1 提取参数（提取表单提交的手机号，短信验证码与密码）
    json_data = request.json
    mobile = json_data.get("mobile", "")
    smscode = json_data.get("smscode", "")
    password = json_data.get("password", "")

    # 2 进行参数验证
    if not all([mobile, smscode, password]):
        return jsonify({"errno": RET.PARAMERR, "errmsg": "参数不足"})

    if not re.match("1[3578][0-9]{9}", mobile):
        return jsonify({"errno": RET.PARAMERR, "errmsg": "手机号格式不对"})

    # 3 逻辑分析

    # 3.1 最好在提交的时候再次判断用户手机号是否被注册，以防止用户在输入短信验证码后修改手机号

    try:
        user = User.query.filter(User.mobile == mobile).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify({"errno": RET.DATAERR, "errmsg": "查询用户手机号出错"})
    if user:
        return jsonify({"errno": RET.PARAMERR, "errmsg": "用户已经注册"})

    # 3.2 判断短信验证码是否正确
    try:
        real_sms_code = redis_store.get("SMS_" + mobile)
        # print(redis_store.get("q"))
    except Exception as e:
        current_app.logger.error(e)
        return jsonify({"errno": RET.DBERR, "errmsg": "获取本地验证码失败"})

    print(real_sms_code)
    # 如果验证码为空
    if not real_sms_code:
        return jsonify({"errno": RET.NODATA, "errmsg": "验证码过期"})

    if real_sms_code != smscode:
        return jsonify({"errno": RET.PARAMERR, "errmsg": "验证码错误"})

    # 获取到验证码后就把数据从redis数据库中删除
    try:
        redis_store.delete("SMS_" + mobile)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify({"errno": RET.DBERR, "errmsg": "redis删除获取到的验证码失败"})

    # 如果验证成功后就创建一个用户对象，添加到数据库中
    user = User()
    user.mobile = mobile
    user.nick_name = mobile
    # 密码要进行加密处理（加密处理可以使用类中set和get的方法去实现）
    user.password = password

    # 添加到数据库中
    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify({"errno": RET.DBERR, "errmsg": "存储数据库出错"})

    # 在redis中存储用户登录状态（此时还没写到注册模块，到时候就知道怎么为什么要这样写了）
    session["user_id"] = user.id
    session["nick_name"] = user.nick_name
    session["mobile"] = user.mobile
    print("ok")

    return jsonify({"errno": RET.OK, "errmsg": "ok"})


# 发送验证码
# 发送验证码也是后端接收请求，操作数据库，然后返回数据，前端进行操作
@passport_bp.route('/image_code')
def get_image_code():
    """获取验证码图片的接口 (GET)"""
    """
    1.获取参数
        1.1 获取code_id全球唯一的uuid编号
    2.参数校验
        2.1 判断code_id是否有值
    3.逻辑处理
        3.1 生成验证码图片&生成验证码图片上面的真实值
        3.2 以code_id作为key将验证码图片上面的真实值存储到redis中
    4.返回值
        4.1 返回图片给前端展示
    """
    # 1 获取参数
    code_id = request.args.get("code_id", "")

    # 2 参数校验
    if not code_id:
        # abort需要导入，主动触发错误
        abort(400)

    # 请求如果带有key，就直接利用工具函数去生成图片验证码
    image_name, real_image_code, image_data = captcha.generate_captcha()
    try:
        # 然后将生成的真实值与请求过来的key存放入redis数据库(只要涉及到数据库操作就要进行异常捕获)
        print("上面" + code_id)
        redis_store.setex("imageCodeId_%s" % code_id, 500, real_image_code)
    except Exception as e:
        # 将错误记录到日志中
        current_app.logger.error(e)

    # 构建响应对象
    response = make_response(image_data)

    # 设置返回的响应体内容，这样能够兼容所有浏览器的数据格式
    response.headers["Content-Type"] = 'image/JPEG'

    return response


# 发送短信验证码
# 这些都是获取一些数据，然后进行判断，然后返回ok，交给前端进行展示
@passport_bp.route("/sms_code", methods=["POST"])
def send_sms_code():
    """点击发送短信验证码后端接口"""
    """
        1.获取参数
             1.1 手机号码mobile，用户填写的图片验证码值image_code，image_code_id全球唯一的UUID编号
         2.校验参数
             2.1 非空判断
             2.2 手机号码格式正则判断
         3.逻辑处理
             3.1 image_code_id编号去redis数据库取出图片验证码的真实值
                 3.1.1 有值： 从redis数据库删除真实值（防止拿着相同的值多次验证）
                 3.1.2 没有值：图片验证码值在redis中过期
             3.2 比较用户填写的图片验证码值和真实的验证码值是否一致
                 TODO: 手机号码有了（用户是否已经注册的判断，用户体验最好），
                 根据手机号码去查询用户是否有注册，有注册，不需要再注册，没有注册才去发送短信验证码
                 一致：填写正确，生成6位的短信验证码值，发送短信验证码
                不一致：提示图片验证码填写错误
             3.3 将生成6位的短信验证码值 存储到redis数据库
         4.返回值
             4.1 发送短信验证码成功
         """
    # 1 获取参数,前端返回回来的是json数据
    params_dict = request.json

    mobile = params_dict.get("mobile")  # 手机号
    image_code = params_dict.get("image_code")  # 图片验证码
    image_code_id = params_dict.get("image_code_id")  # 图片验证码的key

    # 2 校验参数
    if not all([mobile, image_code, image_code_id]):
        # 捕捉错误到日志
        current_app.logger.error("参数不足")
        # 利用flask中自带的jsonify将返回的数据转换为json数据
        return jsonify({"errno": RET.PARAMERR, "errmsg": "参数不足"})

    if not re.match("1[3578][0-9]{9}", mobile):
        current_app.logger.error("手机号格式错误")
        return jsonify({"errno": RET.PARAMERR, "errmsg": "手机号格式错误"})

    # 3 逻辑判断

    # 3.1 上面判断都正确后，然后从数据库中取数据，进行验证码对比（因为是使用数据库，所以要私用异常捕获）
    try:
        real_image_code = redis_store.get("imageCodeId_%s" % image_code_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify({"errno": RET.DATAERR, "errmsg": "读取数据错误"})

    if real_image_code:
        # 如果存在就删除，以防二次验证
        print(image_code_id)
        redis_store.delete("imageCodeId_%s" % image_code_id)
    else:
        return jsonify({"errno": RET.NODATA, "errmsg": "验证码过期"})

    # 3.2 从数据库查询出数据库和请求的数据进行对比
    if image_code.lower() != real_image_code.lower():
        return jsonify(errno=RET.PARAMERR, errmasg="图片验证码书写错误")

    # 如果比对正确从数据库中去查询手机号号码是否已经被注册
    try:
        # user = User.query.filter(User.mobile == mobile).first()
        user = User.query.filter_by(mobile=mobile).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify({"errno": RET.DATAERR, "errmsg": "读取数据错误"})

    # 判断手机号是否已经被注册
    if user:
        return jsonify(errno=RET.DATAEXIST, errmsg="手机号已经被注册")

    # 生成随机的6位数
    sms_code = random.randint(0, 999999)
    sms_code = "%06d" % sms_code

    # 借用第三方库将短信发送给用户
    # try:
    #     ccp = CCP()
    #     # 注意： 测试的短信模板编号为1
    #     result = ccp.send_template_sms('18338556553', [sms_code, 5], 1)
    # except Exception as e:
    #     current_app.logger.error(e)
    #     return jsonify(erron=RET.THIRDERR, errmsg="云通讯发送验证码错误")
    #
    # if result != 0:
    #     return jsonify(erron=RET.THIRDERR, errmsg="第三方发送信息错误")

    # 将生成的6位数验证码存到数据库
    try:
        redis_store.setex("SMS_" + mobile, 300, 123456)  # 这里设置时间要注意一下
        # print(redis_store.get("SMS_" + mobile))
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DATAERR, errmsg="保存验证码到数据库发生异常")

    return jsonify(errno=RET.OK, errmsg="验证码发送成功率")
