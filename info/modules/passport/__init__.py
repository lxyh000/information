from flask import Blueprint

passport_bp = Blueprint("psaaport", __name__, url_prefix="/passport")

from .views import *
