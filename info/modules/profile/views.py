from flask import g, jsonify, request, current_app, session, redirect

from info import db, constants
from info.modules.profile import profile_bp
from info.utits.common import user_login_data
from info.utits.pic_storage import pic_storage
from info.utits.response_code import RET
from flask import render_template
from info.models import Category, News


# 基本登录首页
@profile_bp.route("/info")
@user_login_data
def get_user_info():
    """展示用户信息"""
    user = g.user
    # 让用户退出登录刷新界面然后直接返回首页
    if not user:
        return redirect("/")

    data = {
        "user_info": user.to_dict() if user else []
    }

    return render_template("profile/user.html", data=data)


# 个人基本资料
@profile_bp.route("/base_info", methods=["POST", "GET"])
@user_login_data
def user_base_info():
    """展示用户基本资料"""
    if request.method == "GET":
        user = g.user
        data = {
            "user_info": user.to_dict() if user else []
        }

        return render_template("profile/user_base_info.html", data=data)

    else:
        # 获取参数
        params_dict = request.json
        nick_name = params_dict.get("nick_name")
        signature = params_dict.get("signature")
        gender = params_dict.get("gender")

        user = g.user

        # 校验参数
        if not user:
            return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")
        if not all([nick_name, signature, gender]):
            return jsonify(errno=RET.PARAMERR, errmsg="参数不足")

        if gender not in (['MAN', 'WOMAN']):
            return jsonify(errno=RET.PARAMERR, errmsg="参数有误")

        # 逻辑处理
        user.nick_name = nick_name
        user.signature = signature
        user.gender = gender

        try:
            # 因为是修改数据所以不需要添加，可以直接提交
            db.session.commit()
        except Exception as e:
            current_app.logger.error(e)
            db.session.rollback()
            return jsonify(errno=RET.DBERR, errmsg="数据库保存错误")

        # 将 session 中保存的数据进行实时更新
        session["nick_name"] = nick_name

        # 4. 返回响应
        return jsonify(errno=RET.OK, errmsg="更新成功")


# 头像
@profile_bp.route("/pic_info", methods=["GET", "POST"])
@user_login_data
def pic_info():
    if request.method == "GET":
        user = g.user
        return render_template('profile/user_pic_info.html', data={"user_info": user.to_dict()})
    else:
        user = g.user
        try:
            # 获取上传的文件
            avatar_file = request.files.get("avatar").read()
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.PARAMERR, errmsg="读取上传的文件错误")

        # 将获取的图片文件上传到七牛云上
        try:
            url = pic_storage(avatar_file)
        except Exception as e:
            current_app.logger(e)
            return jsonify(errno=RET.THIRDERR, errmsg="上传到七牛云错误")

        #  将头像信息更新到当前用户的模型中
        user.avatar_url = url

        # 保存到数据库
        try:
            db.session.commit()
        except Exception as e:
            current_app.logger.error(e)
            db.session.rollback()
            return jsonify(errno=RET.DBERR, errmsg="图片路径保存到数据库失败")

        return jsonify(errno=RET.OK, errmsg="ok", data={"avatar_url": constants.QINIU_DOMIN_PREFIX + url})


# 密码修改
@profile_bp.route("/pass_info", methods=["POST", "GET"])
@user_login_data
def pass_info():
    user = g.user
    if request.method == "GET":
        return render_template("profile/user_pass_info.html")
    else:
        # 获取传入的参数
        params_dict = request.json
        old_password = params_dict.get("old_password")
        new_password = params_dict.get("new_password")

        # 校验参数
        if not all([old_password, new_password]):
            return jsonify(errno=RET.PARAMERR, errmsg="参数有误")
        # 核对原始密码
        if not user.check_passowrd(old_password):
            return jsonify(errno=RET.PWDERR, errmsg="原密码错误")

        user.password = new_password

        try:
            db.session.commit()
        except Exception as e:
            current_app.logger.error(e)
            db.session.rollback()
            return jsonify(errno=RET.DBERR, errmsg="保存数据失败")

        return jsonify(errno=RET.OK, errmsg="保存成功")


# 用户收藏
@profile_bp.route("/collection")
@user_login_data
def user_collection():
    # 获取查看的页数，未提交页数会默认为第一页（前端会提交页数参数）
    p = request.args.get("p", 1)

    # 因为要对请求的页数去数据库中查询，所以要对页数进行int
    try:
        p = int(p)
    except Exception as e:
        current_app.logger.error(e)
        # 这里如果请求有错误就直接可以返回第一页就行了，不用给用户返回错误
        p = 1

    # 查询出当前用户，然后去数据库中查询出当前用户的收藏信息，将收藏内容以分页的形式返回
    # 默认数据库中没值的时候返回这些数据，因为这不是报错，所以不能返回错误数据
    user = g.user
    collections = []
    current_page = 1
    total_page = 1
    try:
        # 进行分页数据查询
        paginate = user.collection_news.paginate(p, constants.USER_COLLECTION_MAX_NEWS, False)
        # 获取分页数据
        collections = paginate.items
        # 获取当前页
        current_page = paginate.page
        # 获取总页数
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)

    # 收藏列表
    collection_dict_li = []

    for news in collections:
        collection_dict_li.append(news.to_dict())

    data = {
        "current_page": current_page,
        "total_page": total_page,
        "collections": collection_dict_li

    }

    return render_template('profile/user_collection.html', data=data)


# 用户发布新闻
@profile_bp.route("/news_release", methods=["POST", "GET"])
@user_login_data
def news_release():
    """用户发布新闻界面"""
    if request.method == "GET":
        # 为get请求时，只需要获取新闻分类就可以可以，不过要记得把最新的那个分类给删除
        categories = []
        try:
            categories = Category.query.all()
        except Exception as e:
            current_app.logger.error(e)
            # 此时只要记录一下这个错误就可以，不需要返回给用户错误信息

        # 记住每次返回用户的数据进行字典化
        categories_dict = []
        for category in categories:
            categories_dict.append(category.to_dict())

        categories_dict.pop(0)
        data = {
            "categories": categories_dict
        }

        return render_template("profile/user_news_release.html", data=data)
    else:
        # 获取上传的参数
        source = "个人发布"
        title = request.form.get("title")
        digest = request.form.get("digest")
        content = request.form.get("content")
        index_image = request.files.get("index_image")
        category_id = request.form.get("category_id")

        # 进行参数校验
        if not all([source, title, digest, content, index_image, category_id]):
            return jsonify(errno=RET.PARAMERR, errmsg="参数不足")

        # 读取图片，并把图片保存在七牛云中
        try:
            index_image_url = index_image.read()
        except Exception as e:
            current_app.logger.error(e)
            return jsonify(errno=RET.PARAMERR, errmsg="读取图片数据错误")

        try:
            key = pic_storage(index_image_url)
        except Exception as e:
            current_app.logger(e)
            return jsonify(errno=RET.THIRDERR, errmsg="保存图片到七牛云错误")

        # 验证完参数之后，创建新闻对象，将对象保存到数据库中
        news = News()
        news.source = source
        news.title = title
        news.digest = digest
        news.content = content
        news.index_image_url = constants.QINIU_DOMIN_PREFIX + key
        news.user_id = g.user.id
        news.category_id = category_id

        # 保存新闻的发布状态(新发布的新闻固定为审核状态)
        news.status = 1

        try:
            db.session.add(news)
            db.session.commit()
        except Exception as e:
            current_app.logger.error(e)
            db.session.rollback()
            return jsonify(errno=RET.DBERR, errmsg="保存数据到数据库错误")

        return jsonify(errno=RET.OK, errmsg="新闻审核状态")


# 获取新闻列表
@profile_bp.route("/news_list")
@user_login_data
def news_list():
    # 获取参数
    p = request.args.get("p", "1")
    try:
        p = int(p)
    except Exception as e:
        current_app.logger.error(e)
        p = 1
    # 进行数据库查询
    user = g.user
    try:
        paginate = News.query.filter(News.user_id == user.id).paginate(p, constants.USER_COLLECTION_MAX_NEWS, False)
        news_li = paginate.items
        print(news_li)
        current_page = paginate.page
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询数据库错误")

    news_dict = []
    for news in news_li:
        news_dict.append(news.to_review_dict())

    data = {
        "news_list": news_dict,
        "total_page": total_page,
        "current_page": current_page

    }

    return render_template('profile/user_news_list.html', data=data)
