from flask import session, current_app, jsonify, g

from info.utits.response_code import RET
import functools


def do_index_class(index):
    if index == 1:
        return "first"
    elif index == 2:
        return "second"
    elif index == 3:
        return "third"
    else:
        return ""


def user_login_data(view_func):
    """用户登录成功的装饰器"""

    @functools.wraps(view_func)  # 使用装饰器会修改函数的一些特有属性，为了防止这一现象，使用该装饰器解决
    def wrapper(*args, **kwargs):
        # 装饰器要实现的功能
        user_id = session.get("user_id")  # 数据怎么存的最好怎么取
        user = None  # type: User
        from info.models import User  # 延迟导入
        if user_id:
            try:
                user = User.query.get(user_id)
            except Exception as e:
                current_app.logger.error(e)
                return jsonify({"errno": RET.DBERR, "errmsg": "数据查询错误"})
        g.user = user  # 使用g变量保存数据
        result = view_func(*args, **kwargs)
        return result

    return wrapper
