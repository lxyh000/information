import logging
from flask import Flask, g
from logging.handlers import RotatingFileHandler
from flask_sqlalchemy import SQLAlchemy  # 导入数据库创建数据库对象
from flask_wtf.csrf import CSRFProtect, generate_csrf  # 导入csrf保护机制
from flask_session import Session  # 导入拓展session对象
from config import config_dict
from redis import StrictRedis
from info.utits.common import do_index_class  # 导入过滤器
from info.utits.common import user_login_data
from flask import render_template

"""因为外部需要使用的变量在函数内部无法使用，所以使用懒加载思想先让它们暴露在外部"""

# 可以提前加载一个空的数据库对象（SQLAlchemy自带的方法）
db = SQLAlchemy()

# 自己去创建懒加载思想
# 声明类型
redis_store = None  # type: StrictRedis


def setup_log(config_name):
    """记录日志函数"""
    # 获取配置类
    config_class = config_dict[config_name]

    # 设置日志的记录等级
    logging.basicConfig(level=config_class.LOG_LEVEL)  # 调试debug级

    # 创建日志记录器，指明日志保存的路径、每个日志文件的最大大小(100M)、保存的日志文件个数上限
    file_log_handler = RotatingFileHandler("logs/log", maxBytes=1024 * 1024 * 100, backupCount=10)

    # 创建日志记录的格式: 日志等级 输入日志信息的文件名 行数 日志信息
    formatter = logging.Formatter('%(levelname)s %(filename)s:%(lineno)d %(message)s')

    # 为刚创建的日志记录器设置日志记录格式
    file_log_handler.setFormatter(formatter)

    # 为全局的日志工具对象（flask app使用的）添加日志记录器
    logging.getLogger().addHandler(file_log_handler)


# 使用工厂方法思想将创建app对象封装为一个函数,不能把配置写死，要留一个参数，这样就能够把需要使用的配置传入就可以了
def create_app(config_name):
    # 0. 记录日志信息
    setup_log(config_name)
    # 1 创建app对象
    app = Flask(__name__)
    # 将配置信息关联到app
    config_class = config_dict[config_name]
    app.config.from_object(config_class)

    # 2 创建数据库对象
    # 懒加载思想，延迟加载（这是SQLAlchemy创建数据库对象自带的一种方法）
    db.init_app(app)

    # 3 创建redis对象，下面两种方法都行
    global redis_store

    # decode_responses将二进制转换为字符串
    redis_store = StrictRedis(host=config_class.REDIS_HOST, port=config_class.REDIS_PORT, db=config_class.REDIS_NUM,
                              decode_responses=True)

    # redis_store = config_name.SESSION_REDIS

    # 4 初始化csrf保护机制,对app添加保护机制
    # 保护机制帮我们实现csrf_token的获取:
    #     1.从request的cookies中提取csrf_token
    #     2.从ajax请求的请求头headers中提取X-CSRFToken字段
    #     3.获取到这个两个值然后做比较验证操作
    CSRFProtect(app)

    @app.after_request
    def set_csrftoken(response):
        # 1.生成csrf_token随机值
        csrf_token = generate_csrf()
        # 2.借助response对象设置csrf_token值到cookie中
        response.set_cookie("csrf_token", csrf_token)
        # 3.返回响应对象
        return response

    # 统一显示404界面
    @app.errorhandler(404)
    @user_login_data  # 因为404界面也有用户数据的展示，所以要将用户数据给查询出来
    def error_handle(error):
        """404界面统一显示"""
        user = g.user
        data = {
            "user_info": user.to_dict() if user else None
        }
        return render_template("news/404.html", data=data)

    app.add_template_filter(do_index_class, "do_index_class")

    # 5 初始化拓展session对象
    Session(app)

    # 注册蓝图
    from info.modules.index import index_bp
    app.register_blueprint(index_bp)

    from info.modules.passport import passport_bp
    app.register_blueprint(passport_bp)

    from info.modules.news import news_bp
    app.register_blueprint(news_bp)

    from info.modules.profile import profile_bp
    app.register_blueprint(profile_bp)

    from info.modules.admin import admin_bp
    app.register_blueprint(admin_bp)

    return app
